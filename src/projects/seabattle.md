---
title: Sea Battle
date: 2016
image: https://c2.staticflickr.com/4/3802/32135537783_ca2da3b0cc_c.jpg
desc: autonomous plotter battleships
---

As humans our relationship with numbers, counting, and arithmetic, is closely linked to our relationship with mark‐making.  Once we moved beyond our own body parts for counting, tally marks became the origin of many numeral systems, and are still evident in some present day writing systems.

Sea Battle uses mechanised mark‐making to interpret a popular pastime that emerged as a pencil and paper game in the early 20th century.  By recreating the game at a scale where it is emphasised as an intercourse between machines and not between humans, the mechanical nature of the game is made more apparent, highlighting its computational qualities.  This installation is not just a simulation however, but a performance that seeks to anthropomorphise the two machines within the context of the game.  The drawing machines themselves are a representation of the simulation running invisibly in code, and it is the physical speed of the machines that determines the duration of each game.

The installation consists of two computers running the Sea Battle application, each controlling a plotter. As in the traditional game, for each turn the application analyses the state of the game, chooses a new target and sends this request to the other machine via the Sea Battle server.   After receiving a response it sends these drawing commands to the plotter, along with its opponent’s move, and the game continues.   The simulation generates procedurally, such that every game is unique.

[flickr album](https://www.flickr.com/photos/26287403@N06/albums/72157676933607703)

![](https://c1.staticflickr.com/3/2157/32796220682_9dd435eeb2_c.jpg)
![](https://c1.staticflickr.com/3/2530/32135399893_f6b9b93126_c.jpg)
![](https://c1.staticflickr.com/1/425/32825602261_5237bcc2a0_c.jpg)
![](https://c2.staticflickr.com/4/3951/32825600721_2935525f98_c.jpg)
![](https://c2.staticflickr.com/4/3802/32135537783_ca2da3b0cc_c.jpg)
![](https://c1.staticflickr.com/3/2473/32825709991_a0b6dfe6dd_c.jpg)