---
title: Piccolo
date: 2012
image: https://live.staticflickr.com/7330/12050316405_4e0848a7bc_c_d.jpg
desc: the tiny CNC-bot
video: https://player.vimeo.com/video/1064687023?h=3025b41e3f&title=0&byline=0&portrait=0
---

A pocket-sized open-source CNC platform.

Piccolo is a low-cost kitset device that uses servos to create a mechanism for tinkering with or developing for basic 2D or 3D CNC output.  Applications range from simple drawings, generative designs and data visualations, to small scale laser cutting or 3d printing, depending on the tool head used.  Piccolo's small size, low power and low cost makes it easy to use in almost any environment and in multiples.

Piccolo is available as an open-source design that is simple, quick to assemble, and easy to use, and is entirely composed of digitally manufactured components and inexpensive off-the-shelf hardware.  The project includes Arduino and Processing libraries, to use Piccolo in a variety of ways such as moving autonomously or responding to sensors and data, whilst providing an accessible educational tool and a new output for Processing sketches.

_Collaboration with Greg Saul as [Diatom Studio](https://web.archive.org/web/20190510202346/http://diatom.cc/piccolo), and with [Cheng Xu](https://floating.pt/) and [Huaishu Peng](https://www.huaishu.me/) of [CoDe Lab](https://code.arc.cmu.edu/)_

[piccolo.cc](https://www.piccolo.cc)  
[github](https://github.com/DiatomStudio/Piccolo)  
[flickr album](https://www.flickr.com/photos/diatomstudio/albums/72157639970253343)  
[public flickr group](https://www.flickr.com/groups/1929303@N21/pool/)  
[Codebits VII (2014) presentation](http://videos.sapo.pt/w2gVnvQOTb1sgSDZP0ed)  

![](https://live.staticflickr.com/7330/12050316405_4e0848a7bc_c_d.jpg)
![](https://live.staticflickr.com/2825/12051144606_031dbca09b_c_d.jpg)
![](https://live.staticflickr.com/5550/12137386253_41de2ed8eb_c_d.jpg)
![](https://live.staticflickr.com/5548/12050692074_9eb86fe729_c_d.jpg)

### Events

   - [Tangívies, Residência AZ 2012](http://altlab.org/2011/11/28/residencia-az-2012/), Montemor-o-Novo, Portugal, Jan 2012;
      - http://291project.tumblr.com/
   - TEI 12, Tangible, Embedded and Embodied Interaction, Kingston, Canada, Feb 2012; **Winner, Design Entrepreneur Challenge**
   - Maker Carnival, CMoDA; China Millennium Monument Museum of Digital Arts, Beijing, China, April 2012;