---
title: SketchChair
date: 2010
image: https://live.staticflickr.com/65535/53023560494_dee11efa6e_c_d.jpg
desc: design your own furniture
---

SketchChair is an open-source software tool that aims to allow anyone to easily design and build their own digitally fabricated furniture.

SketchChair lets users design chairs using a simple 2d drawing interface, automatically generating the structure of a chair and testing its stability.   Users can simulate sitting on a chair with a customisable figure of themselves, in order to test and refine the chair to ensure it will comfortably support them.

The software automatically generates cutting profiles for the chairs, which can then be used to make physical SketchChairs.  Using a cnc router, laser cutter or paper cutter, these parts can be cut from any suitable flat sheet material, and then easily assembled by hand.

_Collaboration with Greg Saul as [Diatom Studio](https://web.archive.org/web/20230620142815/http://diatom.cc/sketchchair)_

[sketchchair.cc](http://www.sketchchair.cc)  
[github](https://github.com/DiatomStudio/SketchChair)  
[flickr album](https://www.flickr.com/photos/tiagororke/albums/72177720309544015/)  
[development flickr collection](https://www.flickr.com/photos/gregsaul/collections/72157624162799361)  
[flickr group](https://www.flickr.com/groups/sketchchair/)  

![](https://live.staticflickr.com/65535/53023812045_28030bc2c7_c_d.jpg)
![](https://live.staticflickr.com/65535/53023560499_6c7430637b_c_d.jpg)
![](https://live.staticflickr.com/65535/53023877118_8f5d44eefc_c_d.jpg)
![](https://live.staticflickr.com/65535/53023388681_ac91d9c583_c_d.jpg)
![](https://live.staticflickr.com/65535/53023823210_6bf0217a19_c_d.jpg)
![](https://live.staticflickr.com/65535/53022834602_3d3862e350_c_d.jpg)

Sketchchair started as a collaboration between Greg Saul and the [JST ERATO Design UI Project](http://www.designinterface.jp/en/) in Tokyo during a visiting researcher stay from August 2009 till February 2010.

In 2011 SketchChair was successfully funded as a [Kickstarter Campaign](https://www.kickstarter.com/projects/diatom/sketchchair-furniture-designed-by-you).

### events

   - DMY Internation Design Fetival, Berlin 2010.
   - DiYDA PLAN 2010 in Germany, 2010.
   - Yes We’re Open!, The European Innovation Festival, Belgium, 2010.
   - Miraikan “Open Spiral”, 2010.
   - Maker Carnival, CMoDA; China Millennium Monument Museum of Digital Arts, Beijing, China, April 2012;

### publications

   - SketchChair: An All-in-one Chair Design System for End-users. Greg Saul, Manfred Lau, Jun Mitani, and Takeo Igarashi. 2011. International conference on Tangible, Embedded and Embodied Interaction (TEI2011).
   - Designnet magazine, Nov 2010.
   - Weave magazine, Nov 2010.
