---
title: CAPTCHA the Flag
date: 2021
desc: interactive plotter livestream
image: https://live.staticflickr.com/65535/52416786953_bc7b306526_c_d.jpg
video: https://player.vimeo.com/video/589596533?h=3025b41e3f&title=0&byline=0&portrait=0
---

Capture the Flag is a traditional game where players compete to gain control of each others flags. In CAPTCHA the Flag, the 'flag' represents the 'attention' of a plotter as it moves between locations on a diagram, registering the participation in real time of people interacting remotely with the work, and using IP geolocation to attempt to group these interactions by their relative physical proximity.

At each node of the diagram, the plotter draws a code represented by abstract symbols.  The plotter is then actuated when somebody correctly identifies the code via the web application and livestream.

The plotter attempts to draw nodes representing participants in geographically distant locations at distant locations on the diagram, and likewise will group nodes of those sharing the same IP or in geographic proximity.

Lines are drawn connecting each node in sequence, and the start and end nodes of the diagram are accompanied by a time stamp.  Viewers of the resulting drawing can retrace the history of interaction with the work during its exhibition.

By combining IP geolocation with the physical nature of the plotter, CAPTCHA the Flag attempts to make participants more aware of the physical presence of their device (and the devices of others) within the infrastructure of the internet.

[captchaflag.com](http://captchaflag.com)  
[flickr album](https://www.flickr.com/photos/tiagororke/albums/72177720302760629)  

![](https://live.staticflickr.com/65535/52416719925_bfc70527bd_c_d.jpg)
![](https://live.staticflickr.com/65535/52416716840_75fa6708b5_c_d.jpg)
![](https://live.staticflickr.com/65535/52416557594_11de69dda4_c_d.jpg)
![](https://live.staticflickr.com/65535/52415757792_8a13f1404b_c_d.jpg)
![](https://live.staticflickr.com/65535/52416786923_9cffdcd560_c_d.jpg)


### technical description

   - Node.js web application running on the Heroku cloud platform
      + MaxMind GeoLite2 database for IP geolocation
      + P5.js for front end
   - Custom pen plotter:
      + GRBL firmware on arduino
      + Raspberry Pi running plotter client
      + Raspberry Pi Zero with camera connected to Twitch live streaming service.

### events

   - [Fuori Visioni 6 Atto II](https://www.instagram.com/fuorivisioni/)
   - [ACM Multimedia 2022](https://2022.acmmm.org/)