---
title: Tardigotchi
date: 2009
desc: hybrid live-virtual pet
image: https://farm3.staticflickr.com/2864/9710892480_00a7fce8ec_c.jpg
video: https://player.vimeo.com/video/6846528?h=3025b41e3f&title=0&byline=0&portrait=0
---

This portable brass sphere features two pets: a living organism and an alife avatar.  The microorganism, a Tardigrade, measures half a millimeter in length and lives on a glass slide inside the enclosure.  Meanwhile, an avatar caricature of this creature performs on the digital display, interacting with the user.

Tardigotchi references the popular Tamagotchi toys from the 1990’s, that interestingly encouraged pet-owner behaviour through just a few simple exchanges.  How do these interactions engender emotional attachment? Can feelings of affection blossom from the ritual of assisting the persistence of a pattern? Does biological life make a difference?

_Collaboration with [Doug Easterly](https://www.douglaseasterly.net/) and [Matt Kenyon](https://www.swamp.nu/tardigotchi). Documentation by [Bureau](https://vimeo.com/bureau)._

![](https://farm3.staticflickr.com/2864/9710892480_00a7fce8ec_c.jpg)
![](https://farm4.staticflickr.com/3834/9710896524_29843ac3af_c.jpg)