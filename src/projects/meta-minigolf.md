---
title: Meta-Minigolf
date: 2019
image: https://live.staticflickr.com/7824/45795295984_94753591ab_c_d.jpg
---

This installation uses the accessibility and familiarity of mini golf, as a medium for playful experimentation with augmented reality. Computer vision and projection mapping are using to immerse the player in a virtual, audio-visual experience.

Players can interact however they like, moving the ball on the fairway. When the ball reaches the hole, the environment will change, taking the player through various experiences.

[Expand residency video documentation](https://vimeo.com/323812382)  
[flickr album](https://www.flickr.com/photos/tiagororke/albums/72157705124534055)  
[github](https://github.com/tiago-rorke/meta-minigolf)  

![](https://live.staticflickr.com/4914/45795292124_ee834b3a93_c_d.jpg)
![](https://live.staticflickr.com/7824/45795295984_94753591ab_c_d.jpg)
![](https://live.staticflickr.com/4889/32645932008_c292eb3efc_c_d.jpg)

### events

- Expand art/science/technology residency at the [Espaço do Tempo](https://oespacodotempo.pt/en/home/) in Montemor-o-novo, Portugal, December 2018