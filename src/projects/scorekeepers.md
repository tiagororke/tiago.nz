---
title: Scorekeepers
date: 2017
desc: tally mark machines
image: https://live.staticflickr.com/4334/35838559983_7a0d8d843b_c_d.jpg
video: https://player.vimeo.com/video/334068771?h=3025b41e3f&title=0&byline=0&portrait=0
---

A modular, time based installation and performance consisting of three small drawing machines and a counting device with digital display. Triggered by a pulse sent each second from the device, the machines are engaged in the act of counting via three different common tally mark systems; one predominant in western cultures, one used in cultures influenced by Chinese characters, and one common in many romance-speaking countries. The total count is also rendered to a display as a number in three different bases; binary, decimal, and hexadecimal.  

[flickr album](https://www.flickr.com/photos/tiagororke/albums/72157684242228152)

![](https://live.staticflickr.com/4340/36601086466_a37898db24_c_d.jpg)
![](https://live.staticflickr.com/4395/36509623421_91d9a1aab8_c_d.jpg)
![](https://live.staticflickr.com/4431/36601114166_58ef5f25f6_c_d.jpg)
![](https://live.staticflickr.com/65535/52984221616_153365b932_c_d.jpg)
![](https://live.staticflickr.com/4366/35838550533_34f614011e_c_d.jpg)

### events

   - [xCoAx 2017](http://2017.xcoax.org/), Lisbon, Portugal.
      - [xCoAx 2017 Paper](scorekeepers/xCoAx_2017_paper_113.pdf)