---
title: Travelling Tiles
date: 2024
image: https://live.staticflickr.com/65535/54065113147_d606a823a0_b.jpg
desc: collaborative drawings
---

_Travelling Tiles_ is a project that aims to create opportunities for creative and intercultural exchanges between children from different regions, through drawing and digital fabrication.

Using a web application, participants can create and share their own drawings, as well as edit the work of others. Once published, the drawings can be converted into ceramic tiles using a _pen plotter_.

The digital drawings, published under a creative commons BY-NC-SA license are available to be repurposed and remixed, and participants are encouraged to create their own versions of existing designs.

By contrast, the pen plotter allows the drawings to be made physical and permanent as a tile, where they can be combined with other analog techniques to create an individually expressive copy.

_With special thanks to [Oficina da Criança](https://www.cm-montemornovo.pt/locais/centro-de-animacao-socioeducativa-oficina-da-crianca/), [Ensaios e Diálogos Associação](https://www.e-da.pt/), [Vicarte](https://vicarte.org/) and [FCT](https://www.fct.unl.pt/)_

[azulejos.tiago.nz](https://azulejos.tiago.nz)  

[flickr album](https://www.flickr.com/photos/tiagororke/albums/72177720321207073/)  

![](https://live.staticflickr.com/65535/54065113147_d606a823a0_b.jpg)
![](https://live.staticflickr.com/65535/54066261788_8f87167774_b.jpg)
![](https://live.staticflickr.com/65535/54065110707_cbeaf6b5c7_b.jpg)
![](https://live.staticflickr.com/65535/54088322071_b8f19885be_b.jpg)
![](https://live.staticflickr.com/65535/54304832396_f79e4b4ff9_b.jpg)

### events

   - Art + Technology Residency, PERIPHERA 2024 Festival, Trafaria, Portugal, October 2024
   - Oficina Da Criança; Montemor-o-Novo, Portugal, August 2024