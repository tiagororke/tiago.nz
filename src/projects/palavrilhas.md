---
title: Palavrilhas
date: 2020
desc: collaborative crosswords
image: https://live.staticflickr.com/65535/50887836928_25709af764_c_d.jpg
video: https://player.vimeo.com/video/514734827?h=3025b41e3f&title=0&byline=0&portrait=0
---

Palavrilhas is a generative, interactive and collaborative installation, inspired by the game of crosswords.

There are two ways to participate:

Suggest a word and the corresponding clue - If the word fits somewhere on the page, the clue is added to the game and the squares corresponding to the word are drawn.

Solve an existing clue - If the word is correct, it will be written in the corresponding empty squares.

The game drawn by a pen plotter, whose movements are filmed and broadcast live. Once the paper has been filled, and all of the clues have either been solved or deemed unsolvable, the game finishes and a new game begins on a blank page.

_collaboration with Paulo Andringa_

[instagram](https://www.instagram.com/palavrilhas_jogo/)  
[flickr album](https://www.flickr.com/photos/tiagororke/albums/72157718088790582/)  

![](https://live.staticflickr.com/65535/50887836928_25709af764_c_d.jpg)

The installation is an invitation to anonymous interaction between participants, through a familiar game that allows the creation of narratives, dialogues between players, or just playful nonsense.

Palavrilhas seeks to transform a traditionally solitary past-time into a shared experience. The drawing machine allows every participant to be telepresent with pen on paper, and each able to contribute to the finished design in a constructive (clue making), responsive (clue solving), or destructive (valdalism) way.

![](https://live.staticflickr.com/65535/50887810378_6f572cd7f1_c_d.jpg)
![](https://live.staticflickr.com/65535/50888642682_8ddecc4ab1_c_d.jpg)
![](https://live.staticflickr.com/65535/50888642912_07226c9580_c_d.jpg)

### special thanks

   - Maria Boavida - [Oma Upcycling](https://www.oma-oma.com/)
   - Maurício Martins - [MILL - Makers In Little Lisbon](https://mill.pt/)
   - Daniel Pires - [Maus Habitos](https://www.maushabitos.com/)
   - Tiago Frois - [Oficinas do Convento](http://www.oficinasdoconvento.com/)