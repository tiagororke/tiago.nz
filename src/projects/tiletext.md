---
title: Tiletext
date: 2018
image: https://live.staticflickr.com/958/42230259282_a3d4207380_c_d.jpg
desc: encode messages onto tiles
---

An interactive installation, and system for creating unique designs on ceramic tiles, based on short text messages.

Via a web form, visitors may submit a small phrase to a modified 3D printer. This text is then used to generate a symmetric pattern, using the order and frequency of letters as inputs for the parameters of the pattern. The resulting pattern and text are then drawn onto an unglazed tile using a custom toolhead that deposits pigment onto the tile.

The installation operates as a kind of automated workshop, where people can see previous prints on unglazed tiles as well as examples of those that have been glazed and fired.  After submitting a message, one can take a blank tile from a stack next to the machine, load it into the printer and then watch the pattern that is revealed as the tile is painted on.

[flickr album](https://www.flickr.com/photos/tiagororke/albums/72157696500693554/)

![](https://live.staticflickr.com/958/42230259282_a3d4207380_c_d.jpg)
![](https://live.staticflickr.com/912/28404055968_35b55cf302_c_d.jpg)
![](https://live.staticflickr.com/979/27406335737_9a603850fe_c_d.jpg)
![](https://live.staticflickr.com/961/27406329277_cc3d5b2740_c_d.jpg)
![](https://live.staticflickr.com/911/27406327237_1f388e1016_c_d.jpg)

### events

 - [Temp Studio #2](https://vimeo.com/275237103) 2018, Lisbon.