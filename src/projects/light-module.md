---
title: Light Module
date: 2022
image: https://live.staticflickr.com/65535/53501762741_56dfe1f585_k_d.jpg
desc: light sculpture
---

Experimentation with laboratory glass, and LED filaments and spotlights, exploring novel consrtuction methods.  The resulting sculpture oscillates between the different light sources in varying cyclical patterns.

[flickr album](https://www.flickr.com/photos/tiagororke/albums/72177720314484152/)  

_Collaboration with [Renato Japi](https://cargocollective.com/RenatoJapi/Light-Module)_

_Exhibition photos by Felipe Brage, courtesy of Renato Japi_  

![](https://live.staticflickr.com/65535/53501762741_56dfe1f585_k_d.jpg)  
![](https://live.staticflickr.com/65535/53502187425_0c3c51e583_k_d.jpg)  
![](https://live.staticflickr.com/65535/53500917767_cf9b5ac656_k_d.jpg)  
![](https://live.staticflickr.com/65535/53501804311_7206b1ab7c_k_d.jpg)  
![](https://live.staticflickr.com/65535/53501955563_f8d3ba4d94_k_d.jpg)  
![](https://live.staticflickr.com/65535/53501955573_c2ec1ef26a_k_d.jpg)  
![](https://live.staticflickr.com/65535/53501803981_361b60d461_k_d.jpg)  


### events

- Renato Japi - Slow Gaze, Ever Light; ARTES, Porto, 2022