---
title: I Miss You
date: 2021
image: https://cortex.persona.co/w/600/i/c68c311bf8a3f4334a281a625148977d5ff4eec9695fb5844559af9390f001eb/a.jpg
desc: breath-regulated light
video: https://player.vimeo.com/video/600930481?h=3025b41e3f&title=0&byline=0&portrait=0
---

I miss you is a sensitive light that is controlled by blowing on a suspended filament.

Your breath regulates the intensity of light. You are its dimmer.

A lamp design that seeks to be a therapeutic, we tried to construct it from strictly essential components and with minimum formal intervention, allowing the user to interact with a naked, totally exposed light.

_Collaboration with [Sara Bozzini](https://sarabozzini.com/I-MISS-YOU)_  

[flickr album](https://www.flickr.com/photos/tiagororke/albums/72177720314483562/)  

![](https://cortex.persona.co/w/600/i/d88df73f9a1a0b7669911755b77b338ffc4f288d59c3783f2f76871f9ab251f2/b.jpg)
![](https://cortex.persona.co/w/600/i/c68c311bf8a3f4334a281a625148977d5ff4eec9695fb5844559af9390f001eb/a.jpg)
![](https://cortex.persona.co/w/600/i/de53c3625cadd351b9a56613f972f9c913255581497daccaf77ad04da1ea4620/c.jpg)
![](https://live.staticflickr.com/65535/53502053544_385a402792_k_d.jpg)
![](https://live.staticflickr.com/65535/53502172405_83596289a8_k_d.jpg)
![](https://live.staticflickr.com/65535/53501748321_8ff7dd1847_k_d.jpg)


### technical description

   - 24V flexible LED filament
   - Conductive thread
   - Attiny1626 microcontroller
   - Flexible PCB with accelerometer
   - Multipurpose electrical enclosure as packaging

### events

   - Italy a new collective landscape, curated by Angela Rui, ADI design museum, Milan, 2023; HKDI Gallery，Hong Kong, 2024
   - Sara Bozzini - ALIÁS, Marvila Studios, Lisbon 2023
   - Redistribution of the sensible, V.V.sorry, curated by Suzanne Wu, Casa Ahorita, Mexico City, 2022
   - Fake/authentic, curated by AMA architecture, Galleria Antonia Jannone, Milan, 2021
