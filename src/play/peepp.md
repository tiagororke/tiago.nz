---
title: PEEPP - Portuguese Epic Emoji Poem Project
date: 2018
desc: collaborative translation into emoji
image: https://live.staticflickr.com/65535/53448856216_5478c716c1_k_d.jpg
video: https://player.vimeo.com/video/880123521?h=3025b41e3f&title=0&byline=0&portrait=0
---

Developed for [The New Art Fest](https://thenewartfest.com/) 2018, PEEPP was an interactive installation for the collaborative translation into emoji of the epic poem *Os Lusíadas* by Portuguese author Luís Vaz de Camões.

After the current line of the poem has been translated, it is printed on a small printer and published to Twitter.

*Os Lusíadas* is considered one of he most important works of literature in the Portuguese language and was the first Portuguese epic poem to be published in print.

PEEPP was installed in the window of the historic [Sá da Costa](https://digitalhub.fch.lisboa.ucp.pt/sa-da-costa-a-livraria-centenaria/) bookstore in Lisbon.

_Collaboration with Maurício Martins and Paulo Andringa at [MILL](https://mill.pt/)_ 

[twitter.com/peep_project](https://twitter.com/peep_project)  
[flickr album](https://www.flickr.com/photos/tiagororke/albums/72177720313878206/)

![](https://live.staticflickr.com/65535/53449310345_b184a44275_k_d.jpg)  
![](https://live.staticflickr.com/65535/53447934077_15e0bf7e6f_k_d.jpg)  
![](https://live.staticflickr.com/65535/53447971917_6f7b3be1ec_k_d.jpg)  
![](https://live.staticflickr.com/65535/53447934082_6a032b795a_k_d.jpg)  
![](https://live.staticflickr.com/65535/53449176044_823e0b0571_b_d.jpg)  
