---
title: Nuclear Taco Sensor Helmet Gameshow
date: 2011
image: https://live.staticflickr.com/3707/9705231770_bab5a52e5c_o_d.jpg
---

Winner 1st place Public Choice Awards of the Codebits 2011 48hr Hackathon, Lisbon.

A wearable visualisation of your spicy taco eating competition experience.  Participants were filmed attempting the challenge and the results were presented in the style of a Japanese gameshow.  Sensors included scalp humidity, temperature, and yoghurt flowmeters.

_Collaboration with [Mauricio Martins](https://www.linkedin.com/in/martinsmauricio), [Filipe Cruz](http://twitter.com/psenough), [Tiago Farto](http://twitter.com/xernobyl) and Ferdinand Meier._

[flickr album](https://www.flickr.com/photos/tiagororke/albums/72177720309213104)  

![](http://farm4.staticflickr.com/3707/9705231770_bab5a52e5c_o.jpg)
![](http://farm3.staticflickr.com/2830/9701996171_aab7d08d3f_c.jpg)
