---
title: Plotter Workshops
date: 2016 - ongoing
image: https://live.staticflickr.com/65535/52997116853_07cae01bed_c_d.jpg
video: https://www.youtube.com/embed/6kKaNTFs4Fw
---

Various workshops involving the assembly and introduction of pen plotters, and introduction to vector drawing for digital fabrication.

_Collaborations with Maurício Martins at [MILL](https://mill.pt), [Oficinas do Convento](www.oficinasdoconvento.com/), Oficina da Criança and [Maus Hábitos](https://www.maushabitos.com/)._  
_video: Oficinas do Convento_  
_photos: Tiago Frois, Inês Castanheira_  

[flickr album](https://www.flickr.com/photos/tiagororke/albums/72177720309289209)  
[morgan plotter album](https://photos.google.com/share/AF1QipMfVS5Kud-fkewvBxjG7ezEawL2pXq2pcHzB3cR7iF7fAVCxWAdHPkjNa2W9qPYZA?key=dDYxRlBSbFpLYXZDT0swUFAyLUxLWFgwUlpGSmZB)  

![](https://live.staticflickr.com/65535/52997017445_0dd2087b79_c_d.jpg)
![](https://live.staticflickr.com/65535/52997016895_8492977baf_c_d.jpg)
![](https://live.staticflickr.com/65535/52997116853_07cae01bed_c_d.jpg)
![](https://live.staticflickr.com/65535/52996795449_ca37d41019_c_d.jpg)

### special thanks

 - Tiago Frois, [Oficinas do Convento](www.oficinasdoconvento.com/)
 - Daniel Pires, [Maus Hábitos](https://www.maushabitos.com/)
 - [Inês Castanheira](https://www.inescastanheira.xyz/)
 - Ana Almeida
 - David Silva