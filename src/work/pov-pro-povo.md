---
title: POV-pro-POVO
date: 2017
desc: pixel light painting
image: https://live.staticflickr.com/65535/52996450441_dbbb84ab89_c_d.jpg
video: https://www.youtube.com/embed/7mDzhCIM3r8
---

"POV for the People" is a simple pixel light-painting tool, using Arduino and Processing.

POV-pro-POVO was part of the workshop "Poesia em Pixeis" ("Poetry in Pixels") at Oficina da Criança in Montemor-o-novo, Portugal, where children learned how to solder their own device and program it with messages and drawings using a Processing application.

The workshop included an exploration of poetry on theme of "quando brinco na rua" ("when I play on the street"), with Nuno Cacilhas.

_Collaboration with Maurício Martins at [MILL](https://mill.pt) and [Oficinas do Convento](www.oficinasdoconvento.com/)_  
_video: Oficinas do Convento_  
_photos: Tiago Frois_  

[flickr album](https://www.flickr.com/photos/tiagororke/albums/72177720309277742/with/52996450441/)

![](https://live.staticflickr.com/65535/52996817090_67a5c3759b_c_d.jpg)
![](https://live.staticflickr.com/65535/52996450406_6f1c241b6c_c_d.jpg)
![](https://live.staticflickr.com/65535/52996450411_7075dd3882_c_d.jpg)
![](https://live.staticflickr.com/65535/52995880222_6e3dc4c436_c_d.jpg)
![](https://live.staticflickr.com/65535/52995848662_6b82131878_c_d.jpg)
![](https://live.staticflickr.com/65535/52996450256_2e87261312_c_d.jpg)