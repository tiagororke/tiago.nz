---
title: Trusty
client: Jack the Maker
date: 2018
image: https://live.staticflickr.com/65535/53445930685_9ffb876b56_k_d.jpg
video: https://www.youtube.com/embed/DOo7nPO1OwE
---

*Video documentation by [Jack the Maker](https://jackthemaker.com/portfolio/trusty/)*

Development at Jack the Maker of a functional robotic recreation of the *Trusty* mascot of Portuguese insurance company Fidelidade.

_Collaboration with Theo Kozanitis, Evangelos_Agas, João Magalhães and João Mendonça at Jack the Maker._

Project roles:
 - Electronics - power management, motion control, sensors, communication
 - Motion design
 - Motion controller firmware

[RTP report about Trusty on Criar.pt, Episode 19](https://www.rtp.pt/play/p5046/e392249/criar-pt)  

[flickr album](https://www.flickr.com/photos/tiagororke/albums/72177720313872139/)

![](https://live.staticflickr.com/65535/53444592627_1e8d621748_k_d.jpg)  
![](https://live.staticflickr.com/65535/53445839589_af61a648b7_k_d.jpg)  
![](https://live.staticflickr.com/65535/53445930700_d371e566b7_k_d.jpg)  