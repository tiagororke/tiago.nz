---
title: Wandering Gaze
client: Ana Teresa Vicente
date: 2018
image: https://live.staticflickr.com/65535/53105413996_caff779278_c_d.jpg
video: https://player.vimeo.com/video/351230866?h=03dca74fc4&title=0&byline=0&portrait=0
---

Interactive installation by [Ana Teresa Vicente](https://anateresavicente.com/wandering-gaze).  

While participants observe the printed photo through the viewfinder, their eye movements are being recorded by the device.  Once they step away, a machine behind the photo uses a magnet to drag metal swarf over the photo surface, slowly degrading and erasing it.  After many interactions, the sum of of the gaze of participants begins to become revealed in the image.  

_collaboration with Maurício Martins and Pedro Ângelo at [MILL](https://mill.pt)_

[github](https://github.com/tiago-rorke/wandering-gaze)  
[flickr album](https://www.flickr.com/photos/tiagororke/albums/72177720310375281/)  

![](https://live.staticflickr.com/65535/53105618899_56bb6beea9_k_d.jpg)  
![](https://live.staticflickr.com/65535/53104846517_cd734683fa_c_d.jpg)  
![](https://live.staticflickr.com/65535/53105414066_3c82e7c281_c_d.jpg)  
![](https://live.staticflickr.com/65535/53105618639_2e7fdac7f1_c_d.jpg)  
![](https://live.staticflickr.com/65535/53105917873_fce1db327d_c_d.jpg)  


### events

- Sociedade Nacional de Belas Artes, Lisbon, Portugal, 2018  
- ARS Electronica, POSTCITY Campus Exhibition, Linz, Austria, 2019  
- Format Festival, Derby, UK, 2019  
- Athens Photo Festival, Greece, 2019  
- [Fotomuseum Winterthur](https://www.fotomuseum.ch/en/situations-post/wandering-gaze/), Switzerland, 2020  
- Taipei Photo Festival, Taiwan, 2020  