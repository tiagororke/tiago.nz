---
title: O Anel Do Unicórnio
client: Teatro do Eléctrico
date: 2021
image: https://live.staticflickr.com/65535/53106895492_47051ac6d3_c_d.jpg
---

Design and development of three large format (3x3m) pen plotters for Teatro do Eléctrico, for their opera for children [O Anel Do Unicórnio](https://teatrodoelectrico.com/o-anel-do-unicornio-uma-opera-em-miniatura/).

_collaboration with Maurício Martins and Timóteo Mendes at [MILL](https://mill.pt)_

[flickr album](https://www.flickr.com/photos/tiagororke/albums/72177720310408814/)  
[google photos (Maurício Martins)](https://photos.google.com/share/AF1QipNsCcrNCPJ0MB9oizuFdX83xW289dCAFXzbbQ1Xj7tSPTDwVdq4Vhna_PLrmVy3QQ?key=UGJUM3cteHNqQUFJcnlxTGtpeGZnWTh0Y3J6dm53)  

![](https://live.staticflickr.com/65535/53106895602_6b49158807_c_d.jpg)  
![](https://live.staticflickr.com/65535/53107973103_9c3344324f_c_d.jpg)  
![](https://live.staticflickr.com/65535/53106895492_47051ac6d3_c_d.jpg)  
![](https://live.staticflickr.com/65535/53107880040_fa0458f886_c_d.jpg)  