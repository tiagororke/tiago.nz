---
title: Stoners
client: Marvilab
date: 2019
image: https://live.staticflickr.com/65535/53448881713_ab3ca356fa_k_d.jpg
video: https://www.youtube.com/embed/Yn4YalJVYL8
---

*Video documentation by [Fred Fabrik](https://www.fredfabrik.com/stoners/)*

Development by [Marvilab](https://marvilab.pt/en/lx-stoners/) and Fred Fabrik of serveral interactive elements at the presentation of the *Stoners* collection by Colisão Studios with Asimagra (Portuguese Association of Industrial Marbles), at Moda Lisboa 2019.

In this installation, participants could use wireless marble controllers to manipulate video presentation of the collection, rotating the models and browsing through garments. We used different slitscan time displacement effects as transitions between the videos.

_Collaboration with Erica Jewell and João Mendonça at Marvilab._

[Video of the controllers in action](https://www.youtube.com/watch?v=Pn0X098Ih90) by Erica Jewell

[flickr album](https://www.flickr.com/photos/tiagororke/albums/72177720313903159/)

![](https://live.staticflickr.com/65535/53448753671_6044064c52_k_d.jpg)  
![](https://live.staticflickr.com/65535/53449071754_fd65599106_k_d.jpg)  
![](https://live.staticflickr.com/65535/53447832417_a0e310952c_k_d.jpg)  
