---
title: Allard Prize Award Design
client: The Allard Prize Foundation
date: 2013 - ongoing
image: https://live.staticflickr.com/65535/53503351436_890eb3071e_h_d.jpg
---

In collaboration with Greg Saul as [Diatom Studio](https://web.archive.org/web/20230608150435/http://diatom.cc/allard-prize)

Award design for the [Allard Prize for International Integrity](https://allardprize.org), an initiative of the Peter A. Allard School of Law at the University of British Columbia.  The biennial award recognises courage and leadership in combating corruption, protecting human rights, and promoting transparency and accountability.

The awards are designed with a generative digital process that uses a particle system to create unique forms and patterns that are derived from the geometry of the names each recipient as they appear on the award. This means each award is unique, an part of a growing family of designs with each edition of the Allard Prize.

Symbolically, these clouds of particles represent a complex and chaotic world, wherein the extraordinary individuals and groups recognised by the Allard Prize are stabilising forces that strive to create balance, order and harmony.

The final pieces are produced using SLA 3D printing, with either and electroformed metal finish, or investment cast in bronze.

[flickr album](https://www.flickr.com/photos/tiagororke/albums/72177720314516123/with/53503351436/)  

![](https://live.staticflickr.com/65535/53503499713_997fee6fa6_k_d.jpg)
![](https://live.staticflickr.com/65535/53503775715_3af20352da_k_d.jpg)
![](https://live.staticflickr.com/65535/53503657999_16dee938df_b_d.jpg)
![](https://live.staticflickr.com/65535/53503351176_d5afd0f651_h_d.jpg)