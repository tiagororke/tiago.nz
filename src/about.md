
#### tiago rorke

Designer, artist and maker, usually immersed in prototyping and physical computing.  Passionate about teaching, and empowering people with tools for them to design and build their own creative experiences and better understand their technological world.

Born in Wellington, New Zealand, formally one half of [Diatom](https://web.archive.org/web/20230619073411/http://diatom.cc/), and currently resident at [MILL - Makers in Little Lisbon](https://mill.pt).

contact:  
hello [at] tiago.nz